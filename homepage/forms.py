from django import forms
from .models import kegiatan1,kegiatan2,kegiatan3

class matkulForm(forms.Form):

    mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required': True,
    }))

    sks = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'placeholder':'Jumlah SKS',
        'type':'number',
        'required': True,
    }))


    tahun = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'type' : 'number',
        'placeholder': 'Tahun',
        'required': True,
    }))

    kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Kelas',
        'required': True,
    }))

class kegiatan1forms(forms.ModelForm):
    class Meta :
        model = kegiatan1
        fields = [
            'nama',
        ]

class kegiatan2forms(forms.ModelForm):
    class Meta :
        model = kegiatan2
        fields = [
            'nama',
        ]

class kegiatan3forms(forms.ModelForm):
    class Meta :
        model = kegiatan3
        fields = [
            'nama',
        ]