from django.shortcuts import render, redirect
from .models import Matkul, kegiatan1, kegiatan2, kegiatan3
from .forms import matkulForm, kegiatan1forms, kegiatan2forms, kegiatan3forms


# Create your views here.
def home(request):
    return render(request, 'homePage.html')

def profile(request):
    return render(request, 'profilePage.html')

def hobby(request):
    return render(request, 'hobbyPage.html')

def education(request):
    return render(request, 'educationPage.html')

def sosmed(request):
    return render(request, 'socialmediaPage.html')

def contact(request):
    return render(request, 'contactmePage.html')

def unisub(request):
    if request.method == "POST":
        form = matkulForm(request.POST)
        if form.is_valid():
                u = Matkul.objects.create(mata_kuliah=request.POST['mata_kuliah'],dosen=request.POST['dosen'],sks=request.POST['sks'],tahun=request.POST['tahun'],kelas=request.POST['kelas'])
                u.save()
        return redirect('/unisub')
    else:
        matakuliah = Matkul.objects.all()
        form = matkulForm()
        response = {"matakuliah":matakuliah, 'form' : form}
        return render(request,'unisub.html',response)


def deletematkul(request,pk):
    if request.method == 'POST':
        form = matkulForm(request.POST)
        if form.is_valid():
            formkuliah = Matkul()
            formkuliah.mata_kuliah = form.cleaned_data['mata_kuliah']
            formkuliah.dosen = form.cleaned_data['dosen']
            formkuliah.sks = form.cleaned_data['sks']
            formkuliah.tahun = form.cleaned_data['tahun']
            formkuliah.kelas = form.cleaned_data['kelas']
            formkuliah.save()
        return redirect('/unisub')
    else:
        Matkul.objects.filter(pk=pk).delete()
        matakuliah = Matkul.objects.all()
        form = matkulForm()        

    response = {'form':form, 'matakuliah':matakuliah}
    return render(request, 'unisub.html', response)

def kegiatan (request) :
    if request.method == 'POST' and 'btn_form_game' in request.POST:
            form_game = kegiatan1forms(request.POST)
            if form_game.is_valid():
                form_game.save()
    elif request.method == 'POST' and 'btn_form_belajar' in request.POST:
        form_belajar = kegiatan2forms(request.POST)
        if form_belajar.is_valid():
            form_belajar.save()
    elif request.method == 'POST' and 'btn_form_futsal' in request.POST:
        form_futsal =kegiatan3forms(request.POST)
        if form_futsal.is_valid():
            form_futsal.save()
        
    form_game = kegiatan1forms()
    form_belajar = kegiatan2forms()
    form_futsal = kegiatan3forms()
    peserta_game = kegiatan1.objects.all()
    peserta_belajar = kegiatan2.objects.all()
    peserta_futsal = kegiatan3.objects.all()
    context = {
        'list_peserta_game': peserta_game,
        'list_peserta_belajar':peserta_belajar,
        'list_peserta_futsal':peserta_futsal,
        'form_game':form_game,
        'form_belajar':form_belajar,
        'form_futsal':form_futsal,
        }
    return render(request, 'kegiatanPage.html' ,context)


      
