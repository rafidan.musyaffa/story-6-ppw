from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.urls import reverse
from .models import kegiatan1,kegiatan2,kegiatan3
from .forms import kegiatan1forms,kegiatan2forms,kegiatan3forms

class UnitTest(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_template_yang_digunakan_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('homepage:kegiatan'))
        self.assertTemplateUsed(response, 'kegiatanPage.html')

    # def test_isi_dari_halaman_kegiatan(self):
    #     response = self.client.get(reverse('Forms:kegiatan'))
    #     isi = response.content.decode('utf8')
    #     self.assertIn("Ngeband", isi)
    #     self.assertIn("Coding", isi)
    #     self.assertIn("Sepedaan", isi)
    #     self.assertIn("<input type ='submit' name = 'btn_form_band' value='Tambah Peserta'/>",isi)
    #     self.assertIn("<input type ='submit' name = 'btn_form_coding' value='Tambah Peserta'/>",isi)
    #     self.assertIn("<input type ='submit' name = 'btn_form_sepeda' value='Tambah peserta'/>",isi)
    #     self.assertIn('<form method="POST" action = "" id="form_band" style="margin-top: 20px;">' , isi)
    #     self.assertIn('<form method="POST" action = "" id="form_coding" style="margin-top: 20px;">' , isi)
    #     self.assertIn('<form method="POST" action = "" id="form_sepeda" style="margin-top: 20px;">' , isi)
    #     self.assertIn("Nama", isi)

    def test_models_dari_halaman_kegiatan(self):
        kegiatan1.objects.create(Nama='abc')
        n_game = kegiatan1.objects.all().count()
        self.assertEqual(n_game,1)
        kegiatan2.objects.create(Nama='abc')
        n_belajar = kegiatan2.objects.all().count()
        self.assertEqual(n_belajar,1)
        kegiatan3.objects.create(Nama='abc')
        n_futsal = kegiatan3.objects.all().count()
        self.assertEqual(n_futsal,1)

    def test_form_band_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_game': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_game = kegiatan1forms(data={'Nama':"abc"})
        self.assertTrue(form_game.is_valid())
        self.assertEqual(form_game.cleaned_data['Nama'],"abc")
    
    def test_form_coding_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_belajar': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_belajar = kegiatan2forms(data={'Nama':"abc"})
        self.assertTrue(form_coding.is_valid())
        self.assertEqual(form_coding.cleaned_data['Nama'],"abc")

    def test_form_sepeda_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_futsal': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_futsal = kegiatan3forms(data={'Nama':"abc"})
        self.assertTrue(form_futsal.is_valid())
        self.assertEqual(form_futsal.cleaned_data['Nama'],"abc")